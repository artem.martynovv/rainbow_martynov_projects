package com.itcube;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Main extends JFrame {
    public static final String RESOURCES = "src/main/resources/";
    private final Map<String, Integer> properties = new HashMap<>();

    private final Icon icon = new ImageIcon("");
    public static final Font FONT_COMIC = new Font("Comic Sans MS", Font.BOLD, 20);
    private JMenuItem improvements;
    private JMenuItem shop;
    public JMenuItem about;
    public JMenuItem statistics;
    public JMenuItem cheatcode;
    private JMenuBar menuBar;
    private JButton btnClick;
    private JButton btnexchange;
    private JLabel lblclicks;
    private JLabel lblgoldclicks;
    private JLabel lblexchange;
    private JLabel lblrank;
    public int goldclicks;
    public int clicks;
    public int obmen = 50;
    public int clickstat;

    public void init() {
        setTitle("SuperClicker");
        setResizable(false);
        setSize(new Dimension(400, 500));
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                writeProperties();
            }
        });
        getContentPane().setLayout(null);

        menuBar = new JMenuBar();

        JMenu menu = new JMenu("Menu");
        JMenu help = new JMenu("Help");

        statistics = new JMenuItem("Statistics");
        statistics.addActionListener(e -> openStatistics());
        shop = new JMenuItem("Shop");
        improvements = new JMenuItem("Improvements");
        about = new JMenuItem("About");
        cheatcode = new JMenuItem("CheatCode");
        cheatcode.addActionListener(e -> openCode());

        about.addActionListener(e -> {
            JOptionPane.showMessageDialog(this, "<html> <h2> <i> (c) 2023, Artem.Martynov </i> </h2>",
                    "About SuperClicker", JOptionPane.INFORMATION_MESSAGE);
        });

        menu.add(statistics);
        menu.add(shop);
        menu.add(improvements);
        help.add(about);
        help.add(cheatcode);

        menuBar.add(menu);
        setJMenuBar(menuBar);
        menuBar.add(help);

        btnClick = new JButton();
        btnClick.setBounds(130, 245, 100, 100);
        btnClick.setFont(FONT_COMIC);
        getContentPane().add(btnClick);
        btnClick.addActionListener(e -> ButtonClick());
        btnClick.addActionListener(e -> RankUpdate());

        lblgoldclicks = new JLabel("GoldClicks: " + properties.get("goldclicks"));
        lblgoldclicks.setBounds(-50, 1, 450, 24);
        lblgoldclicks.setHorizontalAlignment(SwingConstants.CENTER);
        lblgoldclicks.setFont(FONT_COMIC);
        getContentPane().add(lblgoldclicks);

        lblclicks = new JLabel("Clicks: " + properties.get("clicks"));
        lblclicks.setBounds(-50, 100, 450, 24);
        lblclicks.setHorizontalAlignment(SwingConstants.CENTER);
        lblclicks.setFont(FONT_COMIC);
        getContentPane().add(lblclicks);

        btnexchange = new JButton("exchange");
        btnexchange.setBounds(130, 170, 100, 24);
        getContentPane().add(btnexchange);
        btnexchange.addActionListener(e -> ExchangeClick());

        lblexchange = new JLabel(properties.get("obmen") + " clicks = 1 goldclick");
        lblexchange.setBounds(-48, 200, 450, 24);
        lblexchange.setHorizontalAlignment(SwingConstants.CENTER);
        getContentPane().add(lblexchange);

        lblrank = new JLabel(" ");
        lblrank.setBounds(100, 1, 450, 24);
        lblrank.setHorizontalAlignment(SwingConstants.CENTER);
        getContentPane().add(lblrank);

        setVisible(true);
    }

    private void openStatistics() {
        Statistics newStatistics = new Statistics();
        newStatistics.frameMain = this;
        newStatistics.init();
    }

    private void openCode() {
        CheatCode newCode = new CheatCode();
        newCode.frameMain = this;
        newCode.init2();
    }

    public void RankUpdate() {
        if (clicks == 0) {
            lblrank.setText("Rank: Beginner");
        }
        if (clicks >= 30) {
            lblrank.setText("Rank: Amateur");
        }
        if (clicks >= 700) {
            lblrank.setText("Rank: Experienced");
        }
    }

    public void ButtonClick() {
        clicks = properties.get("clicks");
        clicks++;
        properties.put("clicks", clicks);

        clickstat = properties.get("clickstat");
        clickstat++;
        properties.put("clickstat", clickstat);

        lblclicks.setText("Clicks: " + clicks);
    }

    public void ExchangeClick() {
        if (clicks >= obmen) {
            clicks -= obmen;

            obmen = properties.get("obmen");
            obmen += 50;
            properties.put("obmen", obmen);

            goldclicks = properties.get("goldclicks");
            goldclicks++;
            properties.put("goldclicks", goldclicks);

            lblexchange.setText(obmen + " clicks = 1 goldclick");
            lblclicks.setText("Clicks: " + clicks);
            lblgoldclicks.setText("GoldClicks: " + goldclicks);
        }
    }

    private void writeProperties() {
        try (FileWriter fileWriter = new FileWriter(RESOURCES + "properties.txt")) {
            fileWriter.write("clicks=" + properties.get("clicks") + "\n" +
                    "clickstat=" + properties.get("clickstat") + "\n" +
                    "obmen=" + properties.get("obmen") + "\n" +
                    "goldclicks=" + properties.get("goldclicks"));

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private void readProperties() {
        try (FileReader fileReader = new FileReader(RESOURCES + "properties.txt");
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {

            String line;
            while ((line = bufferedReader.readLine()) != null) {

                String[] split = line.split("=");
                properties.put(split[0], Integer.parseInt(split[1]));
            }
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static void main(String[] args) {
        Main clicker = new Main();
        clicker.readProperties();
        clicker.init();
        clicker.RankUpdate();
    }
}
