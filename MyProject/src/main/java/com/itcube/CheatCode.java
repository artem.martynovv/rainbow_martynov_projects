package com.itcube;

import javax.swing.*;
import java.awt.*;

public class CheatCode extends JFrame {
    public static final Font FONT_COMIC = new Font("Comic Sans MS", Font.BOLD, 14);
    private JTextField textField;
    private JLabel lblcode;
    Main frameMain;

    public void init2() {
        this.setBounds(150, 160, 250, 200);
        this.setResizable(false);
        this.setTitle("CheatCode");
        this.getContentPane().setLayout(null);

        textField = new JTextField();
        textField.setBounds(70, 70, 100, 24);
        textField.setHorizontalAlignment(JTextField.RIGHT);
        textField.setFont(FONT_COMIC);
        textField.addActionListener(e -> {
            int check = Integer.parseInt(textField.getText());
            if (check == 1234) {
                frameMain.clicks += 200;
            }
        });
        getContentPane().add(textField);

        lblcode = new JLabel("Redeem code");
        lblcode.setBounds(70, 50, 100, 24);
        lblcode.setHorizontalAlignment(SwingConstants.CENTER);
        lblcode.setFont(FONT_COMIC);
        getContentPane().add(lblcode);

        this.setVisible(true);
    }
}