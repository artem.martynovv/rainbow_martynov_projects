package com.itcube;

import javax.swing.*;
import java.awt.*;

public class Statistics extends JFrame {
    public static final Font FONT_COMIC = new Font("Comic Sans MS", Font.BOLD, 14);
    private JLabel lblclickstat;
    private JLabel Beginner;
    private JLabel Amateur;
    Main frameMain;

    public void init() {
        this.setBounds(150, 160, 400, 500);
        this.setResizable(false);
        this.setTitle("Statistics");
        this.getContentPane().setLayout(null);

        lblclickstat = new JLabel("Shared Clicks: " + frameMain.clickstat);
        lblclickstat.setBounds(-140, 1, 450, 24);
        lblclickstat.setHorizontalAlignment(SwingConstants.CENTER);
        lblclickstat.setFont(FONT_COMIC);
        getContentPane().add(lblclickstat);

        Beginner = new JLabel("Beginner = 0 clicks");
        Beginner.setBounds(-140, 30, 450, 24);
        Beginner.setHorizontalAlignment(SwingConstants.CENTER);
        Beginner.setFont(FONT_COMIC);
        getContentPane().add(Beginner);

        Amateur = new JLabel("Amateur = 300 clicks");
        Amateur.setBounds(-133, 50, 450, 24);
        Amateur.setHorizontalAlignment(SwingConstants.CENTER);
        Amateur.setFont(FONT_COMIC);
        getContentPane().add(Amateur);

        Amateur = new JLabel("Experienced = 700 clicks");
        Amateur.setBounds(-133, 50, 450, 24);
        Amateur.setHorizontalAlignment(SwingConstants.CENTER);
        Amateur.setFont(FONT_COMIC);
        getContentPane().add(Amateur);

        this.setVisible(true);
    }
}
